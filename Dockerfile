##
## Copyright (c) 2021 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##
FROM almalinux:8

ARG branch=stable/queens
ARG requirements_branch=stable/queens
ARG python_version=2

RUN set -eux ; \
    export LANG=C.UTF-8 ; \
    dnf install --assumeyes --setopt=install_weak_deps=False \
        python${python_version}-devel \
        python${python_version}-pip \
        git \
        gcc ; \
    git clone https://github.com/openstack/requirements.git --depth 1 --branch ${requirements_branch} ; \
    git clone https://github.com/openstack/heat.git --depth 1 --branch ${branch} ; \
    pip${python_version} install -U pip ; \
    pip${python_version} install -c requirements/upper-constraints.txt PyMySQL python-memcached pymemcache heat/ ; \
    mkdir /etc/heat ; \
    cp /heat/etc/heat/api-paste.ini /etc/heat; \
    dnf remove --assumeyes python${python_version}-devel gcc git ; \
    dnf clean all ; \
    rm -rf /var/cache/dnf ; \
    rm -rf /requirements /heat

CMD ["heat-api"]
